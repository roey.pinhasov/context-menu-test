import urllib
import re
url = "https://the-internet.herokuapp.com/context_menu"
# webtxt = urllib.urlopen(url)
# print webtxt.read()

page = urllib.urlopen(url).read()

str1 = "Right-click in the box below to see one called 'the-internet'"
str2 = "Right-click in the box below to see one called 'the-internet'"
print re.findall(str1, page)
print re.findall(str2, page)

# => via string.find, returns the position ...
isRightClickText = re.findall(str1, page)
IsAlibaba = re.findall(str2, page)

def testRightClick():
    assert len(isRightClickText) > 0

def testIsAlibaba():
    assert len(IsAlibaba) > 0


testRightClick()

testIsAlibaba()